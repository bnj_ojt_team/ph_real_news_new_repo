<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


 Route::get('/', function () {
     return redirect()->route('home.home');
 });

Route::group(['controller'=>'PlatformsController','as'=>'platforms.','prefix'=>'platforms'],function(){
    Route::get('index','index')->name('index');
    Route::get('getPlatforms','getPlatforms')->name('getPlatforms');
    Route::post('add','add')->name('add');
    Route::match(['post','get'],'edit/{id}','edit')->name('edit');
    Route::delete('delete/{id}','delete')->name('delete');
    Route::get('bin','bin')->name('bin');
    Route::get('getPlatformsDeleted','getPlatformsDeleted')->name('getPlatformsDeleted');
    Route::post('restore/{id}','restore')->name('restore');
    Route::delete('forceDelete/{id}','forceDelete')->name('forceDelete');
});

Route::group(['controller'=>'UsersController','as'=>'users.','prefix'=>'users'],function(){
    Route::get('index','index')->name('index');
    Route::get('getUsers','getUsers')->name('getUsers');
    Route::post('add','add')->name('add');
    Route::match(['post','get'],'edit/{id}','edit')->name('edit');
    Route::delete('delete/{id}','delete')->name('delete');
    Route::match(['post','get'],'forgotpassword','forgotpassword')->name('forgotpassword');
    Route::match(['post','get'],'resetpassword/{token}','resetpassword')->name('resetpassword');
});

Route::group(['controller'=>'VideoHeadersController','as'=>'video-headers.','prefix'=>'video-headers'],function(){
    Route::get('index','index')->name('index');
    Route::get('getVideoHeaders','getVideoHeaders')->name('getVideoHeaders');
    Route::post('add','add')->name('add');
    Route::match(['post','get'],'edit/{id}','edit')->name('edit');
    Route::delete('delete/{id}','delete')->name('delete');
    Route::get('bin','bin')->name('bin');
    Route::get('getTrashedVideoHeaders','getTrashedVideoHeaders')->name('getTrashedVideoHeaders');
    Route::post('restore/{id}','restore')->name('restore');
    Route::delete('forceDelete/{id}','forceDelete')->name('forceDelete');
});

Route::group(['controller'=>'VideosController','as'=>'videos.','prefix'=>'videos'],function(){
    Route::get('index','index')->name('index');
    Route::get('getVideos','getVideos')->name('getVideos');
    Route::get('getVideoPlatforms/{id}','getVideoPlatforms')->name('getVideoPlatforms');
    Route::post('add','add')->name('add');
    Route::match(['post','get'],'edit/{id}','edit')->name('edit');
    Route::delete('delete/{id}','delete')->name('delete');
    Route::get('bin','bin')->name('bin');
    Route::get('getVideosDeleted','getVideosDeleted')->name('getVideosDeleted');
    Route::post('restore/{id}','restore')->name('restore');
    Route::delete('forceDelete/{id}','forceDelete')->name('forceDelete');
});

Route::group(['controller'=>'HomeController','as'=>'home.','prefix'=>'home'],function(){
    Route::get('about','about')->name('about');
    Route::get('video/{id}','video')->name('video');
    Route::match(['post','get'],'home','home')->name('home');
    Route::get('autocomplete','autocomplete')->name('autocomplete');
});

// Route::controller(MailerController::class)->prefix('mail')->name('mail.')->group(function(){
//     Route::get('send-mail', 'sendMail')->name('send-mail');
//     Route::get('send-mail-link', 'sendMailLink')->name('send-mail-link');
// });

Auth::routes();


//Route::get('/', [App\Http\Controllers\PlatformsController::class, 'index'])->name('index');

// Route::get('/', [App\Http\Controllers\PlatformsController::class, 'index'])->name('index');

