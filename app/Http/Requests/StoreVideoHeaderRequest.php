<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVideoHeaderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $stopOnFirstFailure = true;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $title = ['sometimes'];
        $description = ['sometimes'];

        if(request()->routeIs('video-headers.add') && request()->isMethod('post')){
            $title = ['required','max:255'];
            $description = ['required','max:65535'];
        }elseif (request()->routeIs('video-headers.edit') && request()->isMethod('post')){
            $title = ['required','max:255'];
            $description = ['required','max:65535'];
        }

        return [
            'title'=>$title,
            'description'=>$description
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>ucwords('Title must not be empty!'),
            'title.max'=>ucwords('Title has invalid length!'),
            'description.required'=>ucwords('description must not be empty!'),
            'description.max'=>ucwords('description has invalid length!'),
        ];
    }

}
