<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $stopOnFirstFailure = true;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $username = ['sometimes'];
        $email = ['sometimes'];
        $password = ['sometimes'];
        $confirmpassword = ['sometimes'];

        if(request()->routeIs('users.add') && request()->isMethod('post')){
            $username = ['required','max:191',Rule::unique('users','username')];
            $email = ['required','max:191',Rule::unique('users','email'),'email'];
            $password = ['required','max:191'];
        }elseif(request()->routeIs('users.edit') && request()->isMethod('post')){
            $username = ['required','max:191',Rule::unique('users','username')->ignore($this->id)];
            $email = ['required','max:191',Rule::unique('users','email')->ignore($this->id),'email'];
            $password = ['sometimes','max:191'];
        }elseif(request()->routeIs('users.forgotpassword') && request()->isMethod('post')){
            $email = ['required','email',Rule::exists('users', 'email') ];
        }elseif (request()->routeIs('users.resetpassword') && request()->isMethod('post')){
            $password = ['required'];
            $confirmpassword = ['required','same:password'];
        }

        return [
            'username'=>$username,
            'email'=>$email,
            'password'=>$password,
            'confirm_password'=>$confirmpassword
        ];
    }

    public function messages()
    {
        return [
            'username.required'=>ucwords('username must not empty!'),
            'username.max'=>ucwords('username lenght invalid!'),
            'username.unique'=>ucwords('this username already exists!'),
            'email.required'=>ucwords('email must not empty!'),
            'email.max'=>ucwords('email lenght invalid!'),
            'email.unique'=>ucwords('this email already exists!'),
            'email.email'=>ucwords('email value invalid!'),
            'email.exists'=>ucwords('Email does not exist!'),
            'password.required'=>ucwords('password must not empty!'),
            'confirm_password.same'=>ucwords('password confirmation not matched!')
        ];
    }

}
