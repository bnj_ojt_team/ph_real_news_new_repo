<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePlatformRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $stopOnFirstFailure = true;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $icon = ['sometimes'];
        $file = ['sometimes'];
        $platformname =['sometimes'];
        $url =['sometimes'];

        if(request()->routeIs('platforms.add') && request()->isMethod('post')){
            $platformname = ['max:191','required',Rule::unique('platforms','platform_name')];
            $file = ['image','required'];
            $url =['required','url','max:255'];
        }else if(request()->routeIs('platforms.edit') && request()->isMethod('post')){
            $platformname = ['max:191','required',Rule::unique('platforms','platform_name')->ignore($this->id)];
            $file = ['image', 'nullable'];
            $url =['required','url','max:255'];
        }

        return [
            'platform_name'=>$platformname,
            'file'=>$file,
            'url'=>$url
        ];
    }

    public function messages()
    {
        return [
            'platform_name.required'=>ucwords('Platform name must not empty!'),
            'platform_name.unique'=>ucwords('Platform name already exists!'),
            'platform_name.max'=>ucwords('Platform name value invalid!'),
            'file.image' => ucfirst('invalid image format, should be in {.jpeg, .png, .jpg} format'),
            'file.required' => ucfirst('image field is required'),
            'url.required' => ucfirst('URL field is required'),
            'url.url' => ucfirst('Invalid URL Format!'),
            'url.max' => ucfirst('URL Charaters exceed!'),
        ];
    }

}
