<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $stopOnFirstFailure = true;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $author = ['sometimes'];
        $file = ['sometimes'];
        $video_header_id = ['sometimes'];
        $video_platforms_url = ['sometimes'];
        $video_platforms = [];
		$video_platforms_platform_id = [];

        if(request()->routeIs('videos.add') && request()->isMethod('post')){
            $author = ['required','max:191'];
            $video_header_id = [Rule::exists('video_headers','id')];
            $file = ['image','required'];
            $video_platforms_url = ['required','url'];
            $video_platforms = ['present'];
			$video_platforms_platform_id = [Rule::exists('platforms', 'id')];
        }else if(request()->routeIs('videos.edit') && request()->isMethod('post')){
            $author = ['required','max:191'];
            $video_header_id = [Rule::exists('video_headers','id')];
            $file = ['image', 'nullable'];
            $video_platforms_url = ['required','url'];
            $video_platforms = ['present'];
			$video_platforms_platform_id = [Rule::exists('platforms', 'id')];
        }

        return [
            'author' => $author,
            'video_header_id' => $video_header_id,
            'file' => $file,
            'video_platforms.*.url' => $video_platforms_url,
            'video_platforms' => $video_platforms,
			'video_platforms.*.platform_id' => $video_platforms_platform_id,
        ];
    }

    public function messages()
    {
        return [
            'author.required' => ucwords('author must not empty!'),
            'author.max' => ucwords('author lenght not valid!'),
            'file.image' => ucfirst('invalid image format, should be in {.jpeg, .png, .jpg} format'),
            'file.required' => ucfirst('image field is required'),
            'video_platforms.*.url.required' => ucfirst("Platform's url field/s is required."),
            'video_platforms.*.url.url' => ucfirst('Input the full URL including the "https:// or http://"!'),
            'video_platforms.present' => ucfirst('Select atleast 1 Platform.'),
            'video_header_id.exists' => ucfirst('Selected Title Does not exist.'),
            'video_platforms.*.platform_id.exists' => ucfirst('Selected Platform Does not exist.'),

        ];
    }

}
