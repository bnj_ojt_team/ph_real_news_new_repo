<?php
// Made by Jexter E. Tomas
namespace App\Http\Traits;

use Illuminate\Support\Facades\DB;

trait CustomSeederTrait{
    public function seed($namespace, $filename){
        $path = storage_path('databases/'.$filename);
        $arr_data = [];
        $file = fopen($path,"r");
        $header = fgetcsv($file);
        $count = count($header) - 1;
        while (($data = fgetcsv($file)) !== FALSE)
        {
            for($i = 0; $i <= $count; $i++){
                if($data[$i] == "NULL"){
                    $arr_data += [$header[$i] => null];
                }else{
                    $arr_data += [$header[$i] => $data[$i]];
                }
            }
            $namespace::create($arr_data);
            $arr_data = [];
        }
    }

    public function seedWithoutModel($database, $filename){
        $path = storage_path('databases/'.$filename);
        $arr_data = [];
        $file = fopen($path,"r");
        $header = fgetcsv($file);
        $count = count($header) - 1;
        while (($data = fgetcsv($file)) !== FALSE)
        {
            for($i = 0; $i <= $count; $i++){
                if($data[$i] == "NULL"){
                    $arr_data += [$header[$i] => null];
                }else{
                    $arr_data += [$header[$i] => $data[$i]];
                }
            }
            DB::table($database)->insert($arr_data);
            $arr_data = [];
        }
    }
}
