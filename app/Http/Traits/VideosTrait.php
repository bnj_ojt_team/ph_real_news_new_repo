<?php
namespace App\Http\Traits;

use App\Models\Platform;

trait VideosTrait{
    public function compareUrl($request){
        $platform_id = $request->input('platform_id');
        $url = $request->input('url');
        $platform_name = Platform::withoutTrashed()->findOrFail($platform_id);
        $urlHost = parse_url($url, PHP_URL_HOST);
        if($urlHost == 'youtu.be' || $urlHost == 'fb.watch'){
            if($urlHost == 'youtu.be'){
                $patern = "/$urlHost/i";
                $newUrl = preg_replace($patern, 'youtube',$urlHost);
                if(strtolower($platform_name->platform_name) != strtolower($newUrl) ){
                    return ['message'=>'This is not '.$platform_name->platform_name.' URL.','result'=>'Error','icon'=>'error'];
                }
            }else{
                $patern = "/$urlHost/i";
                $newUrl = preg_replace($patern, 'facebook',$urlHost);
                if(strtolower($platform_name->platform_name) != strtolower($newUrl) ){
                    return ['message'=>'This is not '.$platform_name->platform_name.' URL.','result'=>'Error','icon'=>'error'];
                }
            } 
        }else{
            if(!stripos(strtolower($urlHost),strtolower($platform_name->platform_name))){
                return ['message'=>'This is not '.$platform_name->platform_name.' URL.','result'=>'Error','icon'=>'error'];
            }
        }
    }
}