<?php

namespace App\Http\Livewire;

use App\Models\Platform;
use App\Models\Video;
use App\Models\VideoHeader;
use App\Models\VideoPlatform;
use Illuminate\Database\Query\Builder;
use Livewire\Component;
use Livewire\WithPagination;
use Carbon\Carbon;

class ShowVideos extends Component
{
    use WithPagination;

    public $search = '';
    public $categories = '';

    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function submit()
    {
        $search = $this->search;
        $text = '';
        $platforms = Platform::orderBy('platform_name', 'asc')->get();
        if ($this->search == '') {

            $text = 'With Most Recent Video Posted';
            $video_headers = VideoHeader::with('Videos')->where('title', 'LIKE', '%'.$this->search.'%')
                // ->whereHas('Videos', function($query) {
                //     $query->whereDate('created_at', '=', Carbon::today()->toDateString())
                //             ->latest()
                //             ->limit(1);
                // })
                ->orderBy(
                    Video::select('created_at')
                        ->whereColumn('video_header_id', 'video_headers.id')
                        ->latest()
                        ->limit(1),
                    'ASC'
                )
                ->take(5)
                ->get();
        }else{
            $text = 'Search Result: ';
            $video_headers = VideoHeader::with('Videos')->where('title', 'LIKE', '%'.$this->search.'%')
                ->take(100)
                ->paginate(10);
        }

        return view('livewire.show-videos',compact('video_headers','platforms','text','search'));

    }

    public function render()
    {
        $search = $this->search;
        $text = '';
        $platforms = Platform::orderBy('platform_name', 'asc')->get();
        if ($this->search == '') {
            // dd('asdsad');
            $text = 'With Most Recent Video Posted';
            $video_headers = VideoHeader::with('Videos')->where('title', 'LIKE', '%'.$this->search.'%')
                // ->whereHas('Videos', function($query) {
                //     $query->whereDate('created_at', '=', Carbon::today()->toDateString())
                //             ->latest()
                //             ->limit(1);
                // })
                ->orderBy(
                    Video::select('created_at')
                        ->whereColumn('video_header_id', 'video_headers.id')
                        ->latest()
                        ->limit(1),
                    'ASC'
                )
                ->take(5)
                ->get();
        }else{
            $text = 'Search Result: ';
            $video_headers = VideoHeader::with('Videos')->where('title', 'LIKE', '%'.$this->search.'%')
                ->take(100)
                ->paginate(10);
            // dd($video_headers);
        }

        return view('livewire.show-videos',compact('video_headers','platforms','text','search'));

    }

}
