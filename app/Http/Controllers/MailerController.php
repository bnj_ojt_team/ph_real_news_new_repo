<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Mailer;
use Mail;

class MailerController extends Controller
{
    public function sendMail()
    {
        $email = '0.coding.is.life.0@gmail.com';
   
        $maildata = [
            'title' => 'Laravel Mail Markdown Link',
        ];
  
        Mail::to($email)->send(new Mailer($maildata));
   
        dd("Mail has been sent successfully");
    }

    public function sendMailLink()
    {
        dd('Nicesnippets.com');
    }
}
