<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVideoHeaderRequest;
use App\Models\VideoHeader;
use Illuminate\Http\Request;

class VideoHeadersController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('VideoHeaders.index');
    }

    public function getVideoHeaders(){
        $data = VideoHeader::withoutTrashed()->get();
        return response()->json(['data'=>$data]);
    }

    public function bin(){
        return view('VideoHeaders.bin');
    }

    public function getTrashedVideoHeaders(){
        $data = VideoHeader::onlyTrashed()->get();
        return response()->json(['data'=>$data]);
    }

    public function add(StoreVideoHeaderRequest $request){
        if($request->isMethod('post')){
            $videoHeader = VideoHeader::make($request->all());
            if($videoHeader->save()){
                $result = ['message'=>'The Video Header has been saved!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The Video Header has been not saved!','result'=>'Error','icon'=>'error'];
                return response()->json($result,404);
            }
        }
    }

    public function edit($id = null, StoreVideoHeaderRequest $request){
        $videoHeader = VideoHeader::withoutTrashed()->findOrFail($id);
        if($request->isMethod('post')){
            $videoHeader->update($request->all());
            if($videoHeader->save()){
                $result = ['message'=>'The Video Header has been saved!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The Video Header has been not saved!','result'=>'Error','icon'=>'error'];
                return response()->json($result,404);
            }
        }
        return response()->json($videoHeader);
    }

    public function delete($id = null){
        $videoHeader = VideoHeader::withoutTrashed()->findOrFail($id);
        $query = VideoHeader::withoutTrashed()->has('Videos')->find($id);
        // dd($query);
        if($query != null){
            $result = ['message'=>'Error Deleting Video Header. This Video Header has associated Video data.!','result'=>'Error','icon'=>'error'];
            return response()->json($result,200);
        }else{
            if($videoHeader->delete()){
                $result = ['message'=>'The Video Header has been deleted!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The Video Header has been not deleted!','result'=>'Error','icon'=>'error'];
                return response()->json($result,404);
            }
        }

    }

    public function restore($id = null){
        $videoHeader = VideoHeader::onlyTrashed()->findOrFail($id);
        if($videoHeader->restore()){
            $result = ['message'=>'The Video Header has been restored!','result'=>'Success','icon'=>'success'];
            return response()->json($result,200);
        }else{
            $result = ['message'=>'The Video Header has been not restored!','result'=>'Error','icon'=>'error'];
            return response()->json($result,404);
        }
    }

    public function forceDelete($id = null){
        $videoHeader = VideoHeader::onlyTrashed()->findOrFail($id);
        if($videoHeader->forceDelete()){
            $result = ['message'=>'The Video Header has been deleted!','result'=>'Success','icon'=>'success'];
            return response()->json($result,200);
        }else{
            $result = ['message'=>'The Video Header has been not deleted!','result'=>'Error','icon'=>'error'];
            return response()->json($result,404);
        }
    }

}
