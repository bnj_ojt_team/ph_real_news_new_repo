<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVideoRequest;
use App\Models\Platform;
use App\Models\Video;
use App\Models\VideoPlatform;
use App\Models\VideoHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Traits\VideosTrait;

class VideosController extends Controller
{
    use VideosTrait;
    public function __construct(){
        $this->middleware('auth');
            // ->except([
            //     'index',
            //     'add',
            //     'edit',
            //     'delete',
            //     'getVideos'
            // ]);
    }

    public function index(){
        $platforms = Platform::withoutTrashed()->get()->map(function ($row){
            return ['value'=>$row->platform_name,'key'=>$row->id];
        })->pluck('value','key');

        $video_headers = VideoHeader::withoutTrashed()->get();
        return view('Videos.index',compact('platforms', 'video_headers'));
    }

    public function getVideoPlatforms($id = null){
        $data = VideoPlatform::query()
            ->with('Platform')
            ->where('video_platforms.video_id','=',$id)
            ->get();
        return response()->json($data);
    }

    public function getVideos(){
        // $data = VideoPlatform::with(['Video', 'Platform'])->get();
        $data = Video::withoutTrashed()->with('VideoPlatforms','VideoPlatforms.Platform', 'VideoHeader')->get();
        return response()->json(['data'=>$data]);
    }

    public function add(StoreVideoRequest $request){
        if($request->isMethod('post')){
            // Trait start
            // if($this->compareUrl($request)){return response()->json($this->compareUrl($request),404);}
            // Trait end
            $video = Video::make($request->all());
            $video->user_id = auth()->user()->id;
            $file = $request->file('file');
            if($request->hasFile('file')){
                $filename = uniqid().$file->getClientOriginalName();
                $path = public_path('img/thumbnail-img');
                if(!File::isDirectory($path)){
                    File::makeDirectory($path);
                }
                $filepath = public_path('img/thumbnail-img');

                if($filename){
                    $file->move($filepath,$filename);
                }
                $video->thumbnail = 'thumbnail-img/'.$filename;
            }else{
                $video->thumbnail = 'thumbnail-img/';
            }

            if($video->save() && $video->videoplatforms()->createMany($request->input('video_platforms'))){
                $result = ['message'=>'The video has been saved!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The video has not been saved!','result'=>'Error','icon'=>'error'];
                return response()->json($result,404);
            }
        }
    }

    public function edit($id = null, StoreVideoRequest $request){
        $video = Video::withoutTrashed()->with(['VideoPlatforms', 'VideoPlatforms.Platform', 'VideoHeader'])->findOrFail($id);
        if($request->isMethod('post')){
            $video->update($request->all());
            $videoPlatform = VideoPlatform::where('video_id','=',$video->id)->forceDelete();
            $file = $request->file('file');
            if($request->hasFile('file')){
                $filename = uniqid().$file->getClientOriginalName();
                $path = public_path('img/'.$video->thumbnail);
                if(File::isFile($path)){
                    File::delete($path);
                }
                $filepath = public_path('img/thumbnail-img');
                if($filename){
                    $file->move($filepath,$filename);
                }
                $video->thumbnail = 'thumbnail-img/'.$filename;
            }
            if($video->save() && $video->videoplatforms()->createMany($request->input('video_platforms'))){
                $result = ['message'=>'The video has been saved!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The video has not been saved!','result'=>'Error','icon'=>'error'];
                return response()->json($result,404);
            }
        }
        return response()->json($video);
    }

    public function delete($id){
        $video = Video::withoutTrashed()->findOrFail($id);
        if($video->delete() && $video->videoplatforms()->delete()){
            $result = ['message'=>'The video has been deleted!','result'=>'Success','icon'=>'success'];
            return response()->json($result,200);
        }else{
            $result = ['message'=>'The video has not been deleted!','result'=>'Error','icon'=>'error'];
            return response()->json($result,404);
        }
    }

    public function bin(){
        return view('Videos.bin');
    }

    public function getVideosDeleted(){
        $data = Video::onlyTrashed()->with('VideoPlatforms','VideoPlatforms.Platform', 'VideoHeader')->get();
        return response()->json(['data'=>$data]);
    }

    public function restore($id = null){
        $video = Video::onlyTrashed()->findOrFail($id);
        if($video->restore() && $video->videoplatforms()->restore()){
            $result = ['message'=>'The video has been deleted!','result'=>'Success','icon'=>'success'];
            return response()->json($result,200);
        }else{
            $result = ['message'=>'The video has not been deleted!','result'=>'Error','icon'=>'error'];
            return response()->json($result,404);
        }
    }

    public function forceDelete($id = null){
        $video = Video::onlyTrashed()->findOrFail($id);
        $path = public_path('img/'.$video->thumbnail);
        if($video->forceDelete() && $video->videoplatforms()->forceDelete()){
            if(File::isFile($path)){
                File::delete($path);
            }
            $result = ['message'=>'The video has been deleted!','result'=>'Success','icon'=>'success'];
            return response()->json($result,200);
        }else{
            $result = ['message'=>'The video has not been deleted!','result'=>'Error','icon'=>'error'];
            return response()->json($result,404);
        }
    }

}
