<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Platform;
use App\Models\VideoPlatform;
use App\Models\Video;
use App\Models\VideoHeader;
use Illuminate\Support\Str;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['home', 'about', 'autocomplete', 'video']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function home()
    {
        return view('Website.home');
    }

    public function about()
    {
        return view('Website.about');
    }

    public function autocomplete(Request $request)
    {
        $data = Video::query()->select("title as value", "id")
                    ->where('title', 'LIKE', '%'. $request->get('search'). '%')
                    ->get();
        return response()->json($data);
    }

    public function video($id = null){
        $video_platforms = VideoPlatform::with('Video.VideoHeader','Video', 'Platform')
                            ->join('videos', 'videos.id', '=', 'video_platforms.video_id')
                            ->orderBy('videos.created_at', 'DESC')
                            ->whereHas('Video.VideoHeader', function ($query) use($id) {
                                $query->where('id', '=', $id);
                            })
                            ->get();

        $videos = VideoHeader::with('Videos')->findOrFail($id);
        $global_id = $id;
        return view('Website.videos', compact('videos', 'video_platforms', 'global_id'));
    }
}
