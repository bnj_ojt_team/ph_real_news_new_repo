<?php

namespace App\Http\Controllers;

use App\Models\Platform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Requests\StorePlatformRequest;

class PlatformsController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('Platforms.index');
    }

    public function getPlatforms(){
        $data = Platform::withoutTrashed()->get();
        return response()->json(['data'=>$data]);
    }

    public function add(StorePlatformRequest $request){
        if($request->isMethod('post')){
            $platform = Platform::make($request->all());
            $platform->user_id = auth()->user()->id;
            $file = $request->file('file');
            if($request->hasFile('file')){
                $filename = uniqid().$file->getClientOriginalName();

                $path = public_path('img/platform-img');
                if(!File::isDirectory($path)){
                    File::makeDirectory($path);
                }

                $filepath = public_path('img/platform-img');

                if($filename){
                    $file->move($filepath,$filename);
                }

                $platform->icon = 'platform-img/'.$filename;
            }else{
                $platform->icon = 'platform-img/';
            }


            if($platform->save()){
                $result = ['message'=>'The platform has been saved!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The platform has not been saved!','result'=>'error','icon'=>'error'];
                return response()->json($result,404);
            }
        }
    }

    public function edit($id = null, StorePlatformRequest $request){
        $platform = Platform::withoutTrashed()->findOrFail($id);
        if($request->isMethod('post')){
            $platform->update($request->all());

            $file = $request->file('file');
            if($request->hasFile('file')){
                $filename = uniqid().$file->getClientOriginalName();

                $path = public_path('img/'.$platform->icon);
                if(File::isFile($path)){
                    File::delete($path);
                }

                $filepath = public_path('img/platform-img');

                if($filename){
                    $file->move($filepath,$filename);
                }

                $platform->icon = 'platform-img/'.$filename;
            }

            if($platform->save()){
                $result = ['message'=>'The platform has been saved!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The platform has not been saved!','result'=>'error','icon'=>'error'];
                return response()->json($result,404);
            }
        }
        return response()->json($platform);
    }

    public function delete($id = null){
        $platform = Platform::withoutTrashed()->findOrFail($id);
        $query = Platform::withoutTrashed()->has('VideoPlatforms')->find($id);
        // dd($query);
        if($query != null){
            $result = ['message'=>'Error Deleting Platform. This Platform has associated Video data.!','result'=>'Error','icon'=>'error'];
            return response()->json($result,200);
        }else{
            if($platform->delete()){
                $result = ['message'=>'The platform has been deleted!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The platform has not been deleted!','result'=>'error','icon'=>'error'];
                return response()->json($result,404);
            }
        }

    }

    public function bin(){
        return view('Platforms.bin');
    }

    public function getPlatformsDeleted(){
        $data = Platform::onlyTrashed()->get();
        return response()->json(['data'=>$data]);
    }

    public function restore($id = null){
        $platform = Platform::onlyTrashed()->findOrFail($id);
        if($platform->restore()){
            $result = ['message'=>'The platform has been restored!','result'=>'Success','icon'=>'success'];
            return response()->json($result,200);
        }else{
            $result = ['message'=>'The platform has not been restored!','result'=>'error','icon'=>'error'];
            return response()->json($result,404);
        }
    }

    public function forceDelete($id = null){
        $platform = Platform::onlyTrashed()->findOrFail($id);
        $path = public_path('img/'.$platform->icon);
        if($platform->forceDelete()){
            if(File::isFile($path)){
                File::delete($path);
            }
            $result = ['message'=>'The platform has been deleted!','result'=>'Success','icon'=>'success'];
            return response()->json($result,200);
        }else{
            $result = ['message'=>'The platform has not been deleted!','result'=>'error','icon'=>'error'];
            return response()->json($result,404);
        }
    }


}
