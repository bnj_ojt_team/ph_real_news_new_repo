<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Mail\ForgotPasswordMail;
use App\Models\User;
use Illuminate\Http\Request;
use Mail;

class UsersController extends Controller
{

    public function __construct(){
        $this->middleware('auth')
            ->except([
                'forgotpassword',
                'resetpassword'
            ]);
    }

    public function index(){
        return view('Users.index');
    }

    public function getUsers(){
        $data = User::get();
        return response()->json(['data'=>$data]);
    }

    public function add(StoreUserRequest $request){
        if($request->isMethod('post')){
            $user = User::make($request->all());
            if($user->save()){
                $result = ['message'=>'The user has been saved!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The user has not been saved!','result'=>'Error','icon'=>'error'];
                return response()->json($result,404);
            }
        }
    }

    public function edit($id = null, StoreUserRequest $request){
        $user = User::findOrFail($id);
        if($request->isMethod('post')){
            $user->update($request->all());
            $user->is_admin = $request->boolean('is_admin');
            if($user->save()){
                $result = ['message'=>'The user has been saved!','result'=>'Success','icon'=>'success'];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The user has not been saved!','result'=>'Error','icon'=>'error'];
                return response()->json($result,404);
            }
        }
        return response()->json($user);
    }

    public function delete($id = null){
        $user = User::findOrFail($id);
        if($user->delete()){
            $result = ['message'=>'The user has been deleted!','result'=>'Success','icon'=>'success'];
            return response()->json($result,200);
        }else{
            $result = ['message'=>'The user has not been deleted!','result'=>'Error','icon'=>'error'];
            return response()->json($result,404);
        }
    }

    public function forgotpassword(StoreUserRequest $request){
        if($request->isMethod('post')){
            $user = User::where('users.email','like','%'.$request->input('email').'%');
            // if($user->count() > 0){
                $user = User::findOrFail($user->firstOrFail()->id);
                $user->token = uniqid().uniqid().uniqid().uniqid();
                Mail::to($user->email)->send(new ForgotPasswordMail($user));
                if($user->save()){
                    $result = ['message'=>'The email has been sent!','result'=>'Success','icon'=>'success'];
                    return response()->json($result,200);
                }else{
                    $result = ['message'=>'The email has not been sent!','result'=>'Error','icon'=>'error'];
                    return response()->json($result,404);
                }
            // }else{
            //     $result = ['message'=>'Please check your email & try again!','result'=>'Error','icon'=>'error'];
            //     return response()->json($result,404);
            // }
        }
        return view('Users.forgotpassword');
    }

    public function resetpassword($token = null, StoreUserRequest $request){
        $user = User::where('users.token','=',$token)
            ->firstOrFail();
        if($request->isMethod('post')){
            $user = User::findOrFail($user->id);
            $user->update($request->all());
            $user->token = uniqid().uniqid().uniqid().uniqid();
            if($user->save()){
                $result = ['message'=>'The password has been changed!','result'=>'Success',
                    'icon'=>'success','redirect'=>url('/login')];
                return response()->json($result,200);
            }else{
                $result = ['message'=>'The password has not been changed!','result'=>'Error','icon'=>'error'];
                return response()->json($result,404);
            }

        }
        return view('Users.resetpassword',compact('user'));
    }

}
