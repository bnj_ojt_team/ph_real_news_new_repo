<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $link = route('users.resetpassword',['token'=>$this->email->token]);
        // return $this->subject('PH REAL NEWS RESET PASSWORD LINK')
        //     ->view('mail.default',compact('link'));

        // return $this->markdown('mail/default')->with('maildata', $this->email);
        return $this->markdown('mail.mail')->with('maildata', $this->details);
    }

}
