<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'video_header_id',
        'author',
        'thumbmail',
        'category'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function User(){
        return $this->belongsTo(User::class);
    }

    public function VideoHeader(){
        return $this->belongsTo(VideoHeader::class,'video_header_id','id');
    }

    public function VideoPlatforms(){
        return $this->hasMany(VideoPlatform::class);
    }

    protected function setCategoryAttribute($value){
        return $this->attributes['category'] = 'NONE';
    }

}
