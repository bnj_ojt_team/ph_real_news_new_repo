<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Platform extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'platform_name',
        'icon',
        'url'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function User(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function VideoPlatforms(){
        return $this->hasMany(VideoPlatform::class,'platform_id','id');
    }

    protected function setPlatformNameAttribute($value){
        return $this->attributes['platform_name'] = ucwords($value);
    }

}
