<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VideoPlatform extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'video_id',
        'platform_id',
        'url',
    ];

    public function Video(){
        return $this->belongsTo(Video::class,'video_id','id');
    }

    public function Platform(){
        return $this->belongsTo(Platform::class,'platform_id','id');
    }

}
