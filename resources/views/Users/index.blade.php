@extends('layouts.admin')
@section('content')
    <style>
        .form-control{
            border-color: #6640b2;
        }
        .form-control:focus{
            border-color: #6640b2;
        }
        .form-control::placeholder{
            font-weight:600;
            color: rgba(0, 0, 0, 0.6);
        }
    </style>
    <!-- Modal -->
    <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" id="form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <label for="username">Username</label>
                                <input type="text" name="username" id="username" class="form-control" placeholder="Username" required />
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <label for="email">Email</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Email" required />
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required />
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="form-check">
                                    <label for="is-admin" class="form-check-label text-muted">
                                        <input type="checkbox" name="is_admin" id="is-admin" value="1" class="form-check-input" />
                                        Admin
                                        <i class="input-helper"></i>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id"/>
                        <button type="reset" class="btn btn-danger rounded-0">
                            Reset
                        </button>
                        <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-default">
                <i class="mdi mdi-library-plus"></i>
            </button>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-hover table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Created At</th>
                                    <th>
                                        <i class="mdi mdi-settings-box"></i>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        $(function () {

            var baseurl = window.location.origin+'/users/';

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                ajax:{
                    url:baseurl+'getUsers',
                    method: 'GET',
                    dataType: 'JSON'
                },
                pagingType: 'full_numbers',
                columnDefs: [
                    {
                        targets: 0,
                        render: function ( data, type, full, meta ) {
                            const row = meta.row;
                            return  row+1;
                        }
                    },
                    {
                        targets: 3,
                        data: null,render: function(data,type,row){
                        return  moment(row.created_at).format('Y/MM/DD h:m A');
                    }
                    },
                    {
                        targets: 4,
                        data: null,render: function(data,type,row){
                        return  '<a href="javascript:void(0)" data-id="'+row.id+'" class="edit"><i class="mdi mdi-lead-pencil"></i></a> | '+
                            '<a href="javascript:void(0)" data-id="'+row.id+'" class="delete"><i class="mdi mdi-eraser"></i></a>';
                    }
                    }
                ],
                columns: [
                    { data: 'id'},
                    { data: 'username'},
                    { data: 'email'},
                    { data: 'created_at'},
                    { data: 'id'}
                ]
            });

            datatable.on('click','.edit',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'edit/'+dataId;
                $.ajax({
                    url:href,
                    type: 'GET',
                    method: 'GET',
                    dataType:'JSON'
                }).done(function (data, status, xhr) {
                    $('#id').val(data.id);
                    $('#email').val(data.email);
                    $('#username').val(data.username);
                    $('#is-admin').prop('checked',data.is_admin);
                    $('#password').prop('disabled',true).fadeOut(100);
                    $('label[for="password"]').fadeOut(100);
                    $('#modal-default').modal('toggle');
                    $('button[type="reset"]').fadeOut(100);
                }).fail(function (xhr, status, error) {
                    const response = JSON.parse(xhr.responseText);
                    swal(response.icon, response.result, response.message);
                });
            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'delete/'+dataId;
                Swal.fire({
                    title: 'Delete File?',
                    text: 'Are You Sure',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        swal(data.icon, data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal(response.icon, response.result, response.message);
                    });
                }
            });
            });

            $('#modal-default').on('hidden.bs.modal', function (e) {
                $('#form')[0].reset();
                $('#id').removeAttr('value');
                $('button[type="reset"]').fadeIn(100);
                $('#password').prop('disabled',false).fadeIn(100);
                $('label[for="password"]').fadeIn(100);
            });

            $('#form').submit(function (e) {
                e.preventDefault();
                const data = new FormData(this);
                var id = $('#id').val();
                const action = id ? baseurl+'edit/'+id: baseurl+'add';
                $.ajax({
                    url: action,
                    method:'POST',
                    type:'POST',
                    data: data,
                    cache:false,
                    contentType: false,
                    processData: false
                }).done(function (data,response, status) {
                    $('#modal-default').modal('toggle');
                    $('#form')[0].reset();
                    table.ajax.reload(null, false);
                    swal(data.icon, data.result, data.message);
                }).fail(function (xhr, status, error) {
                    // console.log(xhr);
                    // console.log(status);
                    // console.log(errror);
                    // alert('message');
                    var messsage = '';
                    var response = JSON.parse(xhr.responseText);
                    var validation = JSON.parse(xhr.responseText).errors;
                    messsage = validation ? validation[Object.keys(validation)[0]]: response.message;
                    swal( 'error',status, messsage);
                });
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon:icon,
                    title:result,
                    text:message,
                    timer:5000
                });
            }

        });
    </script>
@endsection
