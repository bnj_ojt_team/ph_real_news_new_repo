@extends('layouts.login')

@section('content')

    <div class="container-scroller d-flex">
        <div class="container-fluid page-body-wrapper full-page-wrapper d-flex">
            <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
                <div class="row flex-grow">
                    <div class="col-lg-6 d-flex align-items-center justify-content-center">
                        <div class="auth-form-transparent text-left p-3">
                            <div class="brand-logo">
                                <img src="{{asset('images/Philippines.png')}}" alt="logo">
                            </div>
                            <h4>Forgot Password</h4>
                            <h6 class="font-weight-light">Happy to see you again!</h6>
                            <form  action="" method="POST" id="form" inlist="form"  enctype="multipart/form-data" class="pt-3">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail">Email</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend bg-transparent">
                                    <span class="input-group-text bg-transparent border-right-0">
                                    <i class="mdi mdi-email-outline text-primary"></i>
                                    </span>
                                        </div>
                                        <input id="email" type="email" placeholder="Please Enter Your Email" class="form-control @error('username') is-invalid @enderror" name="email" value="{{ old('username') }}" required autocomplete="email" autofocus>
                                        @error('username')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="my-2 d-flex justify-content-between align-items-center">
                                    <a class="auth-link text-black" href="{{ url('/login') }}">
                                        {{ __('Back To Login') }}
                                    </a>
                                </div>
                                <div class="my-3">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 login-half-bg d-none d-lg-flex flex-row">
                        <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2018  All rights reserved.</p>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <script>
        $(function () {

            var baseurl = window.location.origin+'/users/';
            $('#form').submit(function (e) {
                e.preventDefault();
                const data = new FormData(this);
                const action = baseurl+'forgotpassword';
                $.ajax({
                    url: action,
                    method:'POST',
                    type:'POST',
                    data: data,
                    cache:false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        Swal.fire({
                            icon: 'info',
                            title: 'Please Wait...',
                            text: 'Sending Email!',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            button:false
                        })
                    }
                }).done(function (data,response, status) {
                    $('#form')[0].reset();
                    swal(data.icon, data.result, data.message);
                }).fail(function (xhr, status, error) {
                    var messsage = '';
                    var response = JSON.parse(xhr.responseText);
                    var validation = JSON.parse(xhr.responseText).errors;
                    messsage = validation ? validation[Object.keys(validation)[0]]: response.message;
                    swal( 'error',error, messsage);
                });
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon:icon,
                    title:result,
                    text:message,
                    timer:5000
                });
            }
        });
    </script>
@endsection
