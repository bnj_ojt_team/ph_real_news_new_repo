<?php
$categories = [
    'Current Affairs' => 'Current Affairs',
    'Politics' => 'Politics',
    'History' => 'History',
    'Cooking' => 'Cooking',
    'Eating' => 'Eating',
    'Drinking' => 'Drinking',
    'Music' => 'Music',
    'Entertainment' => 'Entertainment',
    'Miscellaneous' => 'Miscellaneous',
];
$checkbox_input_count = 0;
?>
@extends('layouts.admin')
@section('content')
    <style>
        .form-control {
            border-color: #6640b2;
        }

        .form-control:focus {
            border-color: #6640b2;
        }

        .form-control::placeholder {
            font-weight: 600;
            color: rgba(0, 0, 0, 0.6);
        }
    </style>
    <!-- Modal -->
    <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <form action="" id="form" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <img src="{{ asset('img/preview-icon.png') }}" class="img-thumbnail" alt=""
                                            id="image-preview" height="100%" width="100%" />
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <button type="button" class="btn btn-secondary rounded-0 col-12"
                                            id="uploadImageButton">Upload</button>
                                        <input type="file" name="file" id="file"
                                            class="form-control file-upload-info" accept="image/*" style="display: none;" />
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <label for="video_header_id">Select Title</label>
                                        <select name="video_header_id" id="video_header_id" class="form-control" required>
                                            <option value="" style="display: none;">Select Title</option>
                                            @foreach ($video_headers as $video_header)
                                                <option value="{{ $video_header->id }}"
                                                    data-desc="{{ $video_header->description }}">{{ $video_header->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-12">
                                        <label for="description">Title Description</label>
                                        <textarea id="description" class="form-control" cols="30" rows="10" disabled>---No Description---</textarea>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <label for="author">Author</label>
                                        <input type="text" name="author" id="author" class="form-control"
                                            placeholder="Author" required />
                                    </div>
                                    {{-- <div class="col-sm-12 col-md-12 col-lg-12">
                                        <label for="category">Category</label>
                                        <select name="category" id="category" class="form-control" required>
                                            <option value="" style="display: none;">Select Category</option>
                                            @foreach ($categories as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}

                                </div>
                            </div>

                            <div class="col-sm-12 col-md-7 col-lg-7">
                                <div class="row">

                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <label for="">Platform/s</label>
                                        <div class="col-12">
                                            @foreach ($platforms as $key => $value)
                                                <div class="form-group row">
                                                    <label class="form-check-label col-sm-12 col-form-label">
                                                        <input type="checkbox"
                                                            class="form-check-input platform platform-{{ $key }}"
                                                            value="{{ $key }}"
                                                            data-id="{{ $checkbox_input_count }}"
                                                            data-platformid="{{ $key }}"
                                                            id="video_platforms-{{ $checkbox_input_count }}-platform_id">
                                                        {{ $value }}
                                                        <i class="input-helper"></i>
                                                        <input type="text"
                                                            class="form-control url url-{{ $key }}"
                                                            data-platformid="{{ $key }}"
                                                            placeholder="{{ $value }} URL"
                                                            id="video_platforms-{{ $checkbox_input_count }}-url" disabled>
                                                    </label>
                                                </div>
                                                <?php $checkbox_input_count++; ?>
                                            @endforeach
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" />
                        <button type="reset" class="btn btn-danger rounded-0">Reset</button>
                        <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
            <button type="button" class="btn btn-primary float-right" id="openModal">
                <i class="mdi mdi-library-plus"></i>
            </button>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-hover table-bordered dt-responsive nowrap"
                                style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Thumbnail</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Author</th>
                                        <th>Available Platform/s</th>
                                        <th>Available URL/s</th>
                                        <th>
                                            <center>
                                                <i class="mdi mdi-settings-box"></i>
                                            </center>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $(document).on('change', '#video_header_id', function() {
                $('#description').val($(this).find(':selected').data('desc'));
            });

            $('.platform').on('click', function() {
                var data_id = $(this).data('id');
                var data_platformid = $(this).data('platformid');
                if ($(this).is(':checked')) {
                    $('#video_platforms-' + data_id + '-url').attr('name', 'video_platforms[' + data_id +
                        '][url]');
                    $(this).attr('name', 'video_platforms[' + data_id + '][platform_id]');
                    $('#video_platforms-' + data_id + '-url')
                        .attr('class', 'form-control url url-' + data_platformid + '')
                        .val('')
                        .removeAttr('disabled');
                } else {
                    $('#video_platforms-' + data_id + '-url').removeAttr('name');
                    $(this).removeAttr('name');
                    $('#video_platforms-' + data_id + '-url');
                    $('#video_platforms-' + data_id + '-url')
                        .attr('class', 'form-control url-' + data_platformid + '')
                        .val('')
                        .attr('disabled', 'disabled');
                }
            });

            // Upload Image Button
            $('#uploadImageButton').on('click', function() {
                $('#file').click();
            });
            var baseurl = window.location.origin + '/videos/';
            var img = $('#image-preview').attr('src');

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy: true,
                processing: true,
                responsive: true,
                serchDelay: 3500,
                deferRender: true,
                ajax: {
                    url: baseurl + 'getVideos',
                    method: 'GET',
                    dataType: 'JSON'
                },
                pagingType: 'full_numbers',
                columnDefs: [{
                        targets: 0,
                        searchable: false,
                        data: null,
                        render: function(data, type, row) {
                            return '<center class="platforms" data-id="'+row.id+'">' +
                                '<img src="/public/img/' + row.thumbnail +
                                '" class="img-thumbnail" height="80" width="80"/>' +
                                '</center>';
                        }
                    },
                    {
                        targets: 4,
                        orderable: false,
                        data: null,
                        render: function(data, type, row) {
                            var platforms_len = row.video_platforms.length;
                            var values = '';
                            $.each(row.video_platforms, function(index, value) {
                                if (index === (platforms_len - 1)) {
                                    values += value.platform.platform_name;
                                } else {
                                    values += value.platform.platform_name + ' ,';
                                }
                            });
                            return values;
                        }
                    },
                    {
                        targets: 5,
                        orderable: false,
                        data: null,
                        render: function(data, type, row) {
                            var url_len = row.video_platforms.length;
                            var values = '';
                            $.each(row.video_platforms, function(index, value) {
                                if (index === (url_len - 1)) {
                                    values += value.url;
                                } else {
                                    values += value.url + ' /';
                                }
                            });
                            return values;
                        }
                    },
                    {
                        targets: 6,
                        orderable: false,
                        data: null,
                        render: function(data, type, row) {
                            return '<center>' +
                                '<a href="javascript:void(0)" data-id="' + row.id +
                                '" class="edit btn btn-primary" title="Edit">Edit<i class="mdi mdi-lead-pencil"></i></a> | ' +
                                '<a href="javascript:void(0)" data-id="' + row.id +
                                '" class="delete btn btn-danger" title="Delete">Delete<i class="mdi mdi-eraser"></i></a>' +
                                '<center>';
                        }
                    }
                ],
                columns: [{
                        data: 'thumbnail'
                    },
                    {
                        data: 'video_header.title'
                    },
                    {
                        data: 'video_header.description'
                    },
                    {
                        data: 'author'
                    },
                    {
                        data: 'author'
                    },
                    {
                        data: 'author'
                    },
                    {
                        data: 'id'
                    }
                ]
            });

            $(document).on('click', '#openModal', function() {
                $('#exampleModalLabel').html('Add Video');
                $('#modal-default').modal('toggle');
            });

            datatable.on('click', '.edit', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl + 'edit/' + dataId;
                $.ajax({
                    url: href,
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON'
                }).done(function(data, status, xhr) {
                    $('#id').val(data.id);
                    $('button[type="reset"]').fadeOut(100);
                    $('#author').val(data.author);
                    var i = 0;
                    $.map(data.video_platforms, function(data) {
                        $('.platform-' + data.platform_id + '')
                            .prop('checked', true)
                            .attr('name', 'video_platforms[' + i + '][platform_id]');
                        $('.url-' + data.platform_id + '')
                            .val(data.url)
                            .attr('class', 'form-control url url-' + data.platform_id + '')
                            .attr('name', 'video_platforms[' + i + '][url]')
                            .removeAttr('disabled');
                        i += 1;
                    });
                    $('#description').val(data.video_header.description);
                    // $('#category').val(data.category);
                    $('#video_header_id').prop('selectedIndex', data.video_header_id);
                    $('#url').val(data.url);
                    $('#image-preview').attr('src', '/public/img/' + data.thumbnail);
                    $('#exampleModalLabel').html('Edit Video');
                    $('#modal-default').modal('toggle');
                }).fail(function(xhr, status, error) {
                    const response = JSON.parse(xhr.responseText);
                    swal(response.icon, response.result, response.message);
                });
            });

            datatable.on('click', '.delete', function(e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl + 'delete/' + dataId;
                Swal.fire({
                    title: 'Delete File',
                    text: 'Are You Sure?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: href,
                            type: 'DELETE',
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType: 'JSON'
                        }).done(function(data, status, xhr) {
                            swal(data.icon, data.result, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function(xhr, status, error) {
                            const response = JSON.parse(xhr.responseText);
                            swal(response.icon, response.result, response.message);
                        });
                    }
                });
            });

            datatable.on('click','center.platforms',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                platforms(dataId);
            });

            $('#modal-default').on('hidden.bs.modal', function(e) {
                $('#form')[0].reset();
                $('.url').each(function() {
                    var id = $(this).data('platformid');
                    // console.log($(this));
                    $('.platform').removeAttr('name');
                    $(this).attr('class', 'form-control url url-' + id + '')
                        .val('')
                        .removeAttr('name')
                        .attr('disabled', 'disabled');
                });
                $('#id').removeAttr('value');
                $('button[type="reset"]').fadeIn(100);
                $('#image-preview').attr('src', img);
            });

            $('#file').on('change', function(event) {
                var image = URL.createObjectURL(event.target.files[0]);
                $('#image-preview').attr('src', image);
            });

            $('#form').submit(function(e) {
                e.preventDefault();
                const data = new FormData(this);
                var id = $('#id').val();
                const action = id ? baseurl + 'edit/' + id : baseurl + 'add';
                $.ajax({
                    url: action,
                    method: 'POST',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function(data, response, status) {
                    $('#modal-default').modal('toggle');
                    $('#form')[0].reset();
                    table.ajax.reload(null, false);
                    swal(data.icon, data.result, data.message);
                }).fail(function(xhr, status, error) {
                    var messsage = '';
                    var response = JSON.parse(xhr.responseText);
                    var validation = JSON.parse(xhr.responseText).errors;
                    messsage = validation ? validation[Object.keys(validation)[0]] : response
                        .message;
                    swal('error', status, messsage);
                });
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon: icon,
                    title: result,
                    text: message,
                    timer: 5000
                });
            }

            function platforms(id) {
                const url = window.location.origin+'/videos/getVideoPlatforms/'+id;
                $.ajax({
                    url: url,
                    method: 'GET',
                    type: 'GET',
                    dataType:'JSON'
                }).done(function(data, response, status) {
                    var html = '';
                    $.map(data,function (data) {
                        html += '<a href="'+data.url+'" target="_blank" style="margin:5px;">' +
                                    '<img src="/public/img/'+data.platform.icon+'" ' +
                                    'style="border:1px solid black; border-radius: 0.7em;" height="60" width="60" />' +
                                '</a>';
                    });
                    popup(html);
                }).fail(function(xhr, status, error) {
                    swal('error', status, error);
                });
            }

            function popup(html) {
                Swal.fire({
                    title: '<strong>Available Platforms</strong>',
                    html:'<div class="w-100 d-flex justify-content-center align-items-center">'+html+'</div>',
                    showCloseButton: false,
                    showCancelButton: false,
                    showConfirmButton: true,
                    focusConfirm: false
                });
            }

        });
    </script>
@endsection
