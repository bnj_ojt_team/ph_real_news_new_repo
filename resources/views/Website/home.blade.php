@extends('layouts.default')
@section('title','Home')
@section('content')
    <style>
        a:hover{
            font-weight: bold;
            color: black;
        }
        .blue{
            background: blue;
        }
        a:focus{
            font-weight: bold;
            color: black;
        }
        #body-container{
           transition: all 0.5s;
        }
       @media screen and (min-width: 1920px){
           .hero__search__form form input{
               width: 85%;
           }
           #body-container{
               margin-top: 2em;
           }
       }
        @media screen and (min-width: 600px){
            .hero__search__form form input{
               width: 85%;
            }
            .hero__search__form form button{
                width: 15%;
            }
        }
        @media screen and (max-width: 600px){
            .hero__search__form form input{
                width: 85%;
            }
            .hero__search__form form button{
                width: 15%;
                font-weight: bolder;
            }
            #body-container{
                margin-top: 15em;
            }
        }
        @media screen and (min-width: 600px){
            .hero__search__form form input{
                width: 85%;
            }
            .hero__search__form form button{
                width: 15%;
                font-weight: bolder;
            }
        }
        @media screen and (min-width: 400px){
            .hero__search__form form input{
                width: 85%;
            }
            .hero__search__form form button{
                width: 15%;
                font-weight: bolder;
            }

            #web-description {
                padding: 0;
                font-size: .5;
            }
        }
    </style>
    <style>
        #web-description {
            padding: 30px;
            -webkit-backdrop-filter: blur(50px) saturate(90%);
            /* backdrop-filter: blur(10px); */
            backdrop-filter: blur(10px) saturate(70%);
            font-size: 1rem;
            text-align: justify;
            text-indent: 5em;
            color: black;
            font-weight: 300;
            font-style: oblique;
            background: -webkit-linear-gradient(50deg, white, white);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
        #head-strings{
            background: -webkit-linear-gradient(50deg, white, white);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        @media screen and (max-width: 974px){
            #web-description {
                padding: 20px;
                font-size: .7rem;
            }
        }

        @media screen and (max-width: 750px){
            #web-description {
                padding: 15px;
                font-size: .7rem;
            }
        }

        @media screen and (max-width: 600px){
            #web-description {
                padding: 10px;
                font-size: .7rem;
            }
            #body-container{
                margin-top: 0em !important;
            }
        }
    </style>
    @livewireStyles
    <style>
        #web-description {
            padding: 30px;
            -webkit-backdrop-filter: blur(50px) saturate(90%);
            /* backdrop-filter: blur(10px); */
            backdrop-filter: blur(10px) saturate(70%);
            font-size: 1rem;
            text-align: justify;
            text-indent: 5em;
            color: black;
            font-weight: 300;
            font-style: oblique;
            background: -webkit-linear-gradient(50deg, white, white);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
        #head-strings{
            background: -webkit-linear-gradient(50deg, white, white);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        @media screen and (max-width: 974px){
            #web-description {
                padding: 20px;
                font-size: .7rem;
            }
        }

        @media screen and (max-width: 750px){
            #web-description {
                padding: 15px;
                font-size: .7rem;
            }
        }

        @media screen and (max-width: 600px){
            #web-description {
                padding: 10px;
                font-size: .7rem;
            }
            #body-container{
                margin-top: 0em !important;
            }
        }
    </style>
    <section class="hero set-bg"
             style="
                     background: url('{{ asset('images/hero/hero-bg.jpg') }}') contain;
                     background-repeat:no-repeat;
                     height: 20em;
                     "
             data-setbg="{{ asset('images/hero/hero-bg.jpg') }}">
        <div class="container" id="fucking-gay">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hero__text">
                        <div class="section-title">
                            <h2 id="head-strings">Discover The Philippines News</h2>
                            <script>
                                $(function() {

                                    'use strict';

                                    var typed1 = new Typed('#head-strings', {
                                        strings: ['<strong>Discover The Philippines News</strong>'],
                                        typeSpeed: 90,
                                        backSpeed: 0,
                                        fadeOut: true,
                                        smartBackspace: true,
                                        loop: false,
                                        showCursor: false
                                    });

                                    var typed2 = new Typed('#web-description', {
                                        strings: [$('#web-description').html()],
                                        typeSpeed: 0,
                                        backSpeed: 0,
                                        fadeOut: true,
                                        smartBackspace: true,
                                        loop: false,
                                        showCursor: false
                                    });
                                });
                            </script>
                            <div class="container shadow" id="web-description">
                                We will send various information about the Philippines.
                                We publish videos of culture, latest news, events related to the Philippines (dining parties, product sales, music), etc.
                                If you want to travel to the Philippines, if you are interested in everyday life such as cooking and customs, and if you want to know the latest information about the Philippines, please subscribe to the channel.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <livewire:show-videos />
        {{-- @livewire('show-videos') --}}
    @livewireScripts
    <script>
        $(function () {
            $(window).resize(function () {
                console.log($(window).width());
               if($(window).width() < 600 ){
                    $('#submit').html('<i class="fa fa-search" aria-hidden="true"></i>');
               }else{
                   $('#submit').html('<i class="fa fa-search" aria-hidden="true"></i>');
               }
            })
        });
    </script>
@endsection
