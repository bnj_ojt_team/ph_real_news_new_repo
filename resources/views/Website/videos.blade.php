@extends('layouts.default')
@section('title', 'About')
@section('content')

    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-area set-bg" data-setbg="{{ asset('images/breadcrumb/flag.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2 id="head-strings" style="  text-shadow:
     3px  3px 0 #000,
    -1px -1px 0 #000,
     1px -1px 0 #000,
    -1px  1px 0 #000,
     1px  1px 0 #000;"></h2>
                        <script>
                            $(function () {

                                'use strict';

                                var typed2 = new Typed('#head-strings', {
                                    strings: ['{{ $videos->title }}'],
                                    typeSpeed: 90,
                                    backSpeed: 0,
                                    fadeOut: true,
                                    smartBackspace: true,
                                    loop: false,
                                    showCursor: true,
                                    cursorChar: '_',
                                });
                            });
                        </script>
                        <p style="font-weight: bolder; font-size: 1.9em; color: black">
                            <strong>
                                {{ $videos->description }}
                            </strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <section class="work spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Available Video's Link/s</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @if (count($video_platforms) == 0)
                    <div class="container">
                        <center>No available video links... </center>
                    </div>
                @else
                    <?php $o = 1; ?>
                    {{-- VideoPlatform.Video.VideoHeader --}}
                    @foreach ($video_platforms as $video_platform)
                        <div class="col-lg-4 col-md-6">
                            <div class="work__item">
                                <div class="work__item__number">0{{ $o }}.</div>
                                <img src="{{ asset('img/' . $video_platform->video->thumbnail) }}" alt="" width="300" height="200">
                                <p>{{ $video_platform->video->videoheader->title }} by: {{ $video_platform->video->author }}
                                </p>
                                <div class="container-fluid d-flex justify-content-between">
                                    <div class="m-auto">
                                        Posted:
                                        <script>
                                            document.write(moment('{{ $video_platform->video->created_at }}').fromNow());
                                        </script>
                                    </div>
                                    <a href="{{ $video_platform->url }}" target="_blank" class="btn btn-outline-success btn-sm"><img
                                            height="20" width="20"
                                            src="{{ asset('img/' . $video_platform->platform->icon) }}" alt="">
                                        Visit...</a>
                                </div>
                            </div>
                        </div>
                        <?php $o++; ?>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

@endsection
