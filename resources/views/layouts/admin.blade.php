<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        PH Real News -
        @include('element.title')
    </title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('ph_logo.png')}}" />

    <link rel="stylesheet" href="{{asset('vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset( 'css/style.css')}}">

    <script src="{{asset('js/jquery-3.6.3.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script>
    <script src="{{asset('js/sweetalert2.all.js')}}"></script>
    <script src="{{asset('js/sweetalert2.all.min.js')}}"></script>

    <link rel="stylesheet" href="{{asset('datatables/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('datatables/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('fonts/poppins.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

</head>
<body class="sidebar-icon-only">

<div class="container-scroller d-flex">
    <!-- partial:./partials/_sidebar.html -->
    @include('element.admin.sidebar')
<!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:./partials/_navbar.html -->
    @include('element.admin.navbar')
    <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                @include('element.breadcrumbs')
               @yield('content')
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:./partials/_footer.html -->
        @include('element.admin.footer')
        <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>

<script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('vendors/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('js/off-canvas.js')}}"></script>
<script src="{{asset('js/hoverable-collapse.js')}}"></script>
<script src="{{asset('js/dashboard.js')}}"></script>

<script src="{{asset('datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('datatables/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('datatables/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('datatables/js/responsive.bootstrap4.min.js')}}"></script>

</body>
</html>
