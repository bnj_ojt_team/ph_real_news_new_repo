<div>

    <div class="container-fluid" id="body-container">

        <hr class="mt-3 mb-5">

        <div class="container-fluid" style="background: url('{{ asset('images/hero/hero-bg.jpg') }}'); height: 30em;">
            <div class="container">
                <div class="hero__search__form">
                    <form wire:submit.prevent="submit" action="{{ route('home.home') }}" id="form"
                        enctype="multipart/form-data" method="post">
                        @csrf
                        <input type="text" style="width: 85%;" placeholder="Search..." name="search_query"
                            id="search" wire:model="search" />
                        {{-- <div class="select__option"> --}}
                        {{-- <select name="category" id="category" wire:model="categories"> --}}
                        {{-- <option value="">Choose Categories</option> --}}
                        {{-- <option value="Current Affairs">Current Affairs</option> --}}
                        {{-- <option value="Politics">Politics</option> --}}
                        {{-- <option value="History">History</option> --}}
                        {{-- <option value="Cooking">Cooking</option> --}}
                        {{-- <option value="Eating">Eating</option> --}}
                        {{-- <option value="Drinking">Drinking</option> --}}
                        {{-- <option value="Music">Music</option> --}}
                        {{-- <option value="Entertainment">Entertainment</option> --}}
                        {{-- <option value="Miscellaneous">Miscellaneous</option> --}}
                        {{-- </select> --}}
                        {{-- </div> --}}
                        <button type="submit" id="submit"> <i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hero__text">
                            <div class="section-title">
                                <h2>PH REAL NEWS</h2>
                            </div>
                            <ul class="hero__categories__tags">
                                @foreach ($platforms as $platform)
                                    <li>
                                        <a href="{{ $platform->url }}" target="_blank">
                                            <img src="{{ asset('img/' . $platform->icon) }}" alt=""
                                                height="60" width="60" />
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <div class="container d-flex justify-content-center">
            <h3><b>{{ @$text }}</b></h3>
        </div>
        @if (count($video_headers) > 0)
            @foreach ($video_headers as $video_header)
                @if($search == '')
                    @if($video_header->videos()->exists())
                        <div class="container">
                            <div class="card"></div>
                            <div class="card shadow">
                                <div class="card-body">
                                    <h3 class="card-title"><strong>{{ $video_header->title }}</strong></h3>
                                    <p class="card-text">
                                        {{ $video_header->description }}
                                    </p>
                                    <a class="btn btn-outline-dark float-right shadow"
                                        href="{{ route('home.video', $video_header->id) }}">
                                        View More...
                                    </a>
                                </div>
                            </div>
                        </div><br>
                    @endif
                @else
                    <div class="container">
                        <div class="card"></div>
                        <div class="card shadow">
                            <div class="card-body">
                                <h3 class="card-title"><strong>{{ $video_header->title }}</strong></h3>
                                <p class="card-text">
                                    {{ $video_header->description }}
                                </p>
                                <a class="btn btn-outline-dark float-right shadow"
                                    href="{{ route('home.video', $video_header->id) }}">
                                    View More...
                                </a>
                            </div>
                        </div>
                    </div><br>
                @endif

            @endforeach
        @else
            <br><br><br>
            <div class="container">
                <center>
                    <h4>There are no Recent Videos Posted... You can use the search bar to find what you are looking for...</h4>
                </center>
            </div>
        @endif

    </div>
    @if ($search != '')
        <div class="row d-flex justify-content-center align-items-center">
            {{ $video_headers->links() }}
        </div>
    @endif


</div>
