<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="#"><img src="{{asset('images/Philippines.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="header__nav">
                    <nav class="header__menu {{url()->current() != route('home.home') ? 'mobile-menu' :''}}">
                        <ul>
                            {{-- {{ var_dump(un) }} --}}
                            @if (url()->current() == route('home.video', $global_id) && !empty($global_id))
                                <li>
                                    <a href="{{ url()->previous() }}">Back</a>
                                </li>
                            @endif

                           @if(auth()->check())
                                <li>
                                    <a href="{{ route('videos.index') }}">Return To Dashboard</a>
                                </li>
                            @endif
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>

