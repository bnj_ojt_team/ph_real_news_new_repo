<!-- Footer Section Begin -->
<footer class="footer">
    <div class="row">
        <div class="col-lg-12">
            <div class="footer__copyright">
                <div class="footer__copyright__text">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                       <strong>
                           Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved |
                       </strong>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
