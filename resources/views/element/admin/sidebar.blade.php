<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item sidebar-category">
            <p>Navigation</p>
            <span></span>
        </li>
        <li class="nav-item {{url()->current() == route('users.index')? 'active': ''}}">
            <a class="nav-link" href="{{route('users.index')}}">
                <i class="mdi mdi-account-box menu-icon"></i>
                <span class="menu-title">Users</span>
            </a>
        </li>
        <li class="nav-item {{url()->current() == route('platforms.index')? 'active': ''}}">
            <a class="nav-link" href="{{route('platforms.index')}}">
                <i class="mdi mdi-view-quilt menu-icon"></i>
                <span class="menu-title">Platforms</span>
            </a>
        </li>
        <li class="nav-item {{url()->current() == route('video-headers.index')? 'active': ''}}">
            <a class="nav-link" href="{{route('video-headers.index')}}">
                <i class="mdi mdi-video menu-icon"></i>
                <span class="menu-title">Video Headers</span>
            </a>
        </li>
        <li class="nav-item {{url()->current() == route('videos.index')? 'active': ''}}">
            <a class="nav-link" href="{{route('videos.index')}}">
                <i class="mdi mdi-file-video menu-icon"></i>
                <span class="menu-title">Videos</span>
            </a>
        </li>
        <li class="nav-item  {{in_array(url()->current(),[
                                    route('platforms.bin'),
                                    route('videos.bin'),
                                    route('video-headers.bin')
                                    ])? 'active': ''}}">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                <i class="mdi mdi-eraser-variant menu-icon"></i>
                <span class="menu-title">Bin</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse {{in_array(url()->current(),[
                                    route('platforms.bin'),
                                    route('videos.bin'),
                                    route('video-headers.bin')
                                    ])? 'show': ''}}"
                 id="auth">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('platforms.bin')}}">
                            Platforms
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('videos.bin')}}">
                            Videos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('video-headers.bin')}}">
                            Video Headers
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</nav>