@extends('layouts.admin')
@section('content')
    <style>
        .form-control{
            border-color: #6640b2;
        }
        .form-control:focus{
            border-color: #6640b2;
        }
        .form-control::placeholder{
            font-weight:600;
            color: rgba(0, 0, 0, 0.6);
        }
    </style>
    <!-- Modal -->
    <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="" id="form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <img src="{{asset('img/preview-icon.png')}}" alt="" width="400" height="400" class="img-thumbnail" id="image-preview" />
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <label for="file">Upload File</label>
                                        <input type="file" name="file" id="file" class="form-control file-upload-info" accept="image/*"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7 col-lg-7">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <label for="platform-name">Platform Name</label>
                                        <input type="text" name="platform_name" id="platform-name" placeholder="{{ucwords('platform name')}}" class="form-control" required />
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <label for="url">Platform URL</label>
                                        <input type="url" name="url" id="url" placeholder="{{ucwords('URL')}}" class="form-control" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id"/>
                        <button type="reset" class="btn btn-danger rounded-0">
                                Reset
                            </button>
                        <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary rounded-0">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">


        <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
            <button type="button" class="btn btn-primary float-right" id="openModal">
                <i class="mdi mdi-library-plus"></i>
            </button>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                           <div class="table-responsive">
                               <table id="datatable" class="table table-hover table-bordered dt-responsive nowrap" style="width:100%">
                                   <thead>
                                   <tr>
                                       <th>Platform Name</th>
                                       <th>Icon</th>
                                       <th>URL</th>
                                       <th>
                                           <center>
                                               <i class="mdi mdi-settings-box"></i>
                                           </center>
                                       </th>
                                   </tr>
                                   </thead>
                               </table>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        $(function () {
            // alert('{{ route('platforms.index') }}');

            $(document).on('click', '#openModal', function(){
                $('#exampleModalLabel').html('Add Platform');
                $('#modal-default').modal('toggle');
            });

            var baseurl = window.location.origin+'/platforms/';
            var img = $('#image-preview').attr('src');

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                ajax:{
                    url:baseurl+'getPlatforms',
                    method: 'GET',
                    dataType: 'JSON'
                },
                pagingType: 'full_numbers',
                columnDefs: [
                    {
                        targets: 1,
                        searchable:false,
                        orderable:false,
                        data: null,render: function(data,type,row){
                            return '<center>'+
                                '<img src="/public/img/'+row.icon+'" height="40" width="40" class="img-thumbnail"/>' +
                                '<center>';
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        data: null,render: function(data,type,row){
                        return '<a href="'+row.url+'" target="_blank">'+row.url+'</a>';
                    }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        data: null,render: function(data,type,row){
                            return '<center>'+
                                '<a href="javascript:void(0)" data-id="'+row.id+'" class="btn btn-primary btn-sm edit" title="Edit">Edit<i class="mdi mdi-lead-pencil"></i></a> | '+
                                '<a href="javascript:void(0)" data-id="'+row.id+'" class="btn btn-danger btn-sm delete" title="Delete">Delete<i class="mdi mdi-eraser"></i></a>' +
                                '<center>';
                        }
                    }
                ],
                columns: [
                    { data: 'platform_name'},
                    { data: 'icon'},
                    { data: 'url'},
                    { data: 'id'}
                ]
            });

            datatable.on('click','.edit',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'edit/'+dataId;
                $.ajax({
                    url:href,
                    type: 'GET',
                    method: 'GET',
                    dataType:'JSON'
                }).done(function (data, status, xhr) {
                    $('#id').val(data.id);
                    $('#platform-name').val(data.platform_name);
                    $('#image-preview').attr('src','/public/img/'+data.icon);
                    $('#exampleModalLabel').html('Edit Platform');
                    $('#url').val(data.url);
                    $('#modal-default').modal('toggle');
                    $('button[type="reset"]').fadeOut(100);
                }).fail(function (xhr, status, error) {
                    const response = JSON.parse(xhr.responseText);
                    swal(response.icon, response.result, response.message);
                });
            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'delete/'+dataId;
                Swal.fire({
                    title: 'Delete File',
                    text: 'Are You Sure?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        swal(data.icon, data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal(response.icon, response.result, response.message);
                    });
                }
            });
            });

            $('#modal-default').on('hidden.bs.modal', function (e) {
                $('#form')[0].reset();
                $('#id').removeAttr('value');
                $('button[type="reset"]').fadeIn(100);
                $('#image-preview').attr('src',img);
            });

            $('#file').on('change',function (event) {
                var image = URL.createObjectURL(event.target.files[0]);
                $('#image-preview').attr('src',image);
            });

            $('#form').submit(function (e) {
                e.preventDefault();
                const data = new FormData(this);
                var id = $('#id').val();
                const action = id ? baseurl+'edit/'+id: baseurl+'add';
                $.ajax({
                    url: action,
                    method:'POST',
                    type:'POST',
                    data: data,
                    cache:false,
                    contentType: false,
                    processData: false
                }).done(function (data,response, status) {
                    $('#modal-default').modal('toggle');
                    $('#form')[0].reset();
                    table.ajax.reload(null, false);
                    swal(data.icon, data.result, data.message);
                }).fail(function (xhr, status, error) {
                    var messsage = '';
                    var response = JSON.parse(xhr.responseText);
                    var validation = JSON.parse(xhr.responseText).errors;
                    messsage = validation ? validation[Object.keys(validation)[0]]: response.message;
                    swal( 'error',status, messsage);
                });
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon:icon,
                    title:result,
                    text:message,
                    timer:5000
                });
            }

        });
    </script>
@endsection
