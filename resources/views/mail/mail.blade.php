
@component('mail::layout')
@slot('header')
@component('mail::header', ['url' => route('users.resetpassword', $maildata['token'])])
<!-- change something but nothing happens -->
© {{ date('Y') }} PH REAL NEWS PASSWORD RESET LINK

@endcomponent
@endslot

@slot('subcopy')
@component('mail::subcopy')
# {{ $maildata['username'] }}

Hello Admin. 
Click this button below to continue to password reset page,
@component('mail::button', ['url' => route('users.resetpassword', $maildata['token'])])
Reset Link
@endcomponent
{{ config('app.name') }}
@endcomponent
@endslot

@slot('footer')
@component('mail::footer')
Footer
@endcomponent
@endslot
@endcomponent
