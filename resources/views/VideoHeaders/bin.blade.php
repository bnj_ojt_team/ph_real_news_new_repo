@extends('layouts.admin')
@section('content')
    <style>
        .form-control{
            border-color: #6640b2;
        }
        .form-control:focus{
            border-color: #6640b2;
        }
        .form-control::placeholder{
            font-weight:600;
            color: rgba(0, 0, 0, 0.6);
        }
    </style>
    <!-- Modal -->
    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-hover table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Deleted At</th>
                                    <th>
                                        <center>
                                            <i class="mdi mdi-settings-box"></i>
                                        </center>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        $(function () {

            var baseurl = window.location.origin+'/video-headers/';

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                ajax:{
                    url:baseurl+'getTrashedVideoHeaders',
                    method: 'GET',
                    dataType: 'JSON'
                },
                pagingType: 'full_numbers',
                columnDefs: [
                    {
                        targets: 2,
                        orderable: false,
                        data: null,render: function(data,type,row){
                        return moment(row.deleted_at).format('YYYY/MM/D h:m A');
                    }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        data: null,render: function(data,type,row){
                        return '<center>'+
                            '<a href="javascript:void(0)" data-id="'+row.id+'" class="btn btn-primary btn-sm restore" title="Restore">Restore<i class="mdi mdi-reload"></i></a> | '+
                            '<a href="javascript:void(0)" data-id="'+row.id+'" class="btn btn-danger btn-sm delete" title="Delete">Delete<i class="mdi mdi-eraser"></i></a>' +
                            '<center>';
                    }
                    }
                ],
                columns: [
                    { data: 'title'},
                    { data: 'description'},
                    { data: 'id'},
                    { data: 'id'}
                ]
            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'forceDelete/'+dataId;
                Swal.fire({
                    title: 'Delete Permanently',
                    text: 'Are You Sure?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        swal(data.icon, data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal(response.icon, response.result, response.message);
                    });
                }
            });
            });

            datatable.on('click','.restore',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'restore/'+dataId;
                Swal.fire({
                    title: 'Restore File',
                    text: 'Are You Sure?',
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON'
                    }).done(function (data, status, xhr) {
                        swal(data.icon, data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal(response.icon, response.result, response.message);
                    });
                }
            });
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon:icon,
                    title:result,
                    text:message,
                    timer:5000
                });
            }

        });
    </script>
@endsection
