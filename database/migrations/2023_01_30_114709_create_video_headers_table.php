<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_headers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->datetime('deleted_at')->nullable();
        });

        // $videoHeadersTableData = array(
        //     array(1, 'Programming Tutorial', 'How to create CRUD using laravel', '2023-01-31 00:50:31', '2023-01-31 00:50:31', NULL),
        //     array(2, 'Computer Operations', 'How to use excel, powerpoint and word', '2023-01-31 00:53:13', '2023-01-31 00:53:13', NULL)
        // );

        // $data = [];
        // $count = 0;
        // foreach ($videoHeadersTableData as $video_header) {
        //     $data[] = [
        //         'id' => $video_header[$count++],
        //         'title' => $video_header[$count++],
        //         'description' => $video_header[$count++],
        //         'created_at' => $video_header[$count++],
        //         'updated_at' => $video_header[$count++],
        //         'deleted_at' => $video_header[$count++],
        //     ];
        //     $count = 0;
        // }

        // DB::table('video_headers')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_headers');
    }
}
