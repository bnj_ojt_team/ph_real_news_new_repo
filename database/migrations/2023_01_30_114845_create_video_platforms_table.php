<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Video;
use App\Models\Platform;

class CreateVideoPlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_platforms', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Video::class);
            $table->foreignIdFor(Platform::class);
            $table->string('url');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->datetime('deleted_at')->nullable();
        });

        // $videoPlatformsTableData = array(
        //     array(6, 1, 1, 'https://www.youtube.com', '2023-01-31 07:54:51', '2023-01-31 07:54:51', NULL),
        //     array(7, 1, 2, 'http://www.fb.com', '2023-01-31 07:54:51', '2023-01-31 07:54:51', NULL)
        // );

        // $data = [];
        // $count = 0;
        // foreach ($videoPlatformsTableData as $video_platform) {
        //     $data[] = [
        //         'id' => $video_platform[$count++],
        //         'video_id' => $video_platform[$count++],
        //         'platform_id' => $video_platform[$count++],
        //         'url' => $video_platform[$count++],
        //         'created_at' => $video_platform[$count++],
        //         'updated_at' => $video_platform[$count++],
        //         'deleted_at' => $video_platform[$count++],
        //     ];
        //     $count = 0;
        // }

        // DB::table('video_platforms')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_platforms');
    }
}
