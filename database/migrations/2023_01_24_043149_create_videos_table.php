<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Platform;
use App\Models\User;
use App\Models\VideoHeader;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->foreignIdFor(VideoHeader::class);
            $table->string('author');
            $table->string('thumbnail');
            $table->string('category')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->datetime('deleted_at')->nullable();
        });

        // $videosTableData = array(
        //     array(1, 2, 1, 'Rodrigo Cabotaje', 'thumbnail-img/63d8bb064fcc9download.jpg', 'Miscellaneous', '2023-01-31 06:53:58', '2023-01-31 07:54:12', NULL)
        // );

        // $data = [];
        // $count = 0;
        // foreach ($videosTableData as $video) {
        //     $data[] = [
        //         'id' => $video[$count++],
        //         'user_id' => $video[$count++],
        //         'video_header_id' => $video[$count++],
        //         'author' => $video[$count++],
        //         'thumbnail' => $video[$count++],
        //         'category' => $video[$count++],
        //         'created_at' => $video[$count++],
        //         'updated_at' => $video[$count++],
        //         'deleted_at' => $video[$count++],
        //     ];
        //     $count = 0;
        // }

        // DB::table('videos')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
