<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

class CreatePlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platforms', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->string('platform_name');
            $table->string('icon');
            $table->string('url');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->datetime('deleted_at')->nullable();
        });
//
//        $platformsTableData = array(
//            array(1, 2, 'Youtube', 'platform-img/63d7582930b76youtube.png', '2023-01-30 05:39:53', '2023-01-30 05:39:53', NULL),
//            array(2, 2, 'Facebook', 'platform-img/63d758335ccddfacebook.png', '2023-01-30 05:40:03', '2023-01-30 05:40:03', NULL),
//            array(3, 2, 'Reddit', 'platform-img/63d86611739ddreddit.png', '2023-01-31 00:51:29', '2023-01-31 00:51:29', NULL),
//            array(4, 2, 'Twitter', 'platform-img/63d866291288btwitter.png', '2023-01-31 00:51:53', '2023-01-31 00:51:53', NULL),
//            array(5, 2, 'Tiktok', 'platform-img/63d8663725491tiktok.png', '2023-01-31 00:52:07', '2023-01-31 00:52:07', NULL),
//            array(6, 2, 'Instagram', 'platform-img/63d86644d11b8instagram.png', '2023-01-31 00:52:20', '2023-01-31 00:52:20', NULL)
//        );
//
//        $data = [];
//        $count = 0;
//        foreach ($platformsTableData as $platform) {
//            $data[] = [
//                'id' => $platform[$count++],
//                'user_id' => $platform[$count++],
//                'platform_name' => $platform[$count++],
//                'icon' => $platform[$count++],
//                'created_at' => $platform[$count++],
//                'updated_at' => $platform[$count++],
//                'deleted_at' => $platform[$count++],
//            ];
//            $count = 0;
//        }
//
//        DB::table('platforms')->insert($data);

        // $platformsTableData = array(
        //     array(1, 2, 'Youtube', 'platform-img/63d7582930b76youtube.png', '2023-01-30 05:39:53', '2023-01-30 05:39:53', NULL),
        //     array(2, 2, 'Facebook', 'platform-img/63d758335ccddfacebook.png', '2023-01-30 05:40:03', '2023-01-30 05:40:03', NULL),
        //     array(3, 2, 'Reddit', 'platform-img/63d86611739ddreddit.png', '2023-01-31 00:51:29', '2023-01-31 00:51:29', NULL),
        //     array(4, 2, 'Twitter', 'platform-img/63d866291288btwitter.png', '2023-01-31 00:51:53', '2023-01-31 00:51:53', NULL),
        //     array(5, 2, 'Tiktok', 'platform-img/63d8663725491tiktok.png', '2023-01-31 00:52:07', '2023-01-31 00:52:07', NULL),
        //     array(6, 2, 'Instagram', 'platform-img/63d86644d11b8instagram.png', '2023-01-31 00:52:20', '2023-01-31 00:52:20', NULL)
        // );

        // $data = [];
        // $count = 0;
        // foreach ($platformsTableData as $platform) {
        //     $data[] = [
        //         'id' => $platform[$count++],
        //         'user_id' => $platform[$count++],
        //         'platform_name' => $platform[$count++],
        //         'icon' => $platform[$count++],
        //         'created_at' => $platform[$count++],
        //         'updated_at' => $platform[$count++],
        //         'deleted_at' => $platform[$count++],
        //     ];
        //     $count = 0;
        // }

        // DB::table('platforms')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platforms');
    }
}
