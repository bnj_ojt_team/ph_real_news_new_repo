<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('token','255')->nullable();
            $table->integer('is_admin');
            $table->rememberToken();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->datetime('deleted_at')->nullable();
        });

        // $usersTableData = array(
        //     array(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$Vj3mPq2DLgGkL7sBlwELUOzJy87QGgTXnGhaPLoOMN7OMCzADUsA6', NULL, 1, 'Pkdps226RCQ6WCYpsCxaSZGHfBUBNdA2p9CnmgcKDySp7y4OuvjPPrvZ4qtQ', '2023-01-23 13:38:22', '2023-01-31 03:08:49', NULL),
        //     array(2, 'jextertomas', 'jextertomas@gmail.com', NULL, '$2y$10$ZgnPjf27FJzp/6f9ydvH0e.uQ.EL5xHjsmufkTDaBKe9GPi.8MKTG', '63d377c2de7b863d377c2deba063d377c2def8863d377c2df370', 1, 'Pibk3RBo122UJi4XTlunpwAcUapdizZ3p6Wf8ubhW6zPghvJ0rhm7V5hcYCi', '2023-01-26 05:07:02', '2023-01-31 07:33:50', NULL)
        // );

        // $data = [];
        // $count = 0;
        // foreach ($usersTableData as $user) {
        //     $data[] = [
        //         'id' => $user[$count++],
        //         'username' => $user[$count++],
        //         'email' => $user[$count++],
        //         'email_verified_at' => $user[$count++],
        //         'password' => $user[$count++],
        //         'token' => $user[$count++],
        //         'is_admin' => $user[$count++],
        //         'remember_token' => $user[$count++],
        //         'created_at' => $user[$count++],
        //         'updated_at' => $user[$count++],
        //         'deleted_at' => $user[$count++],
        //     ];
        //     $count = 0;
        // }

        // DB::table('users')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
