<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Traits\CustomSeederTrait;

class PersonalAccessTokensSeeder extends Seeder
{
    use CustomSeederTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedWithoutModel('personal_access_tokens', 'personal_access_tokens.csv');
    }
}
