<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Traits\CustomSeederTrait;

class UserSeeder extends Seeder
{
    use CustomSeederTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->seed('App\Models\User', 'users.csv');
        $this->seedWithoutModel('users', 'users.csv');
    }
}
