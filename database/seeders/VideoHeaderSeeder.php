<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Traits\CustomSeederTrait;

class VideoHeaderSeeder extends Seeder
{
    use CustomSeederTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('App\Models\VideoHeader', 'video_headers.csv');
    }
}
