<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            UserSeeder::class,
            VideoSeeder::class,
            VideoPlatformSeeder::class,
            VideoHeaderSeeder::class,
            PlatformSeeder::class,
            PasswordResetsSeeder::class,
            FailedJobsSeeder::class,
            PersonalAccessTokensSeeder::class,
        ]);
    }
}
