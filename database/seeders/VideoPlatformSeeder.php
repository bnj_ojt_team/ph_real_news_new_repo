<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Traits\CustomSeederTrait;

class VideoPlatformSeeder extends Seeder
{
    use CustomSeederTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('App\Models\VideoPlatform', 'video_platforms.csv');
    }
}
